$(document).ready(function() {

    localStorage.setItem('msg', '…');
    $('#msg').text(localStorage.msg);
    alert('Cortex dit : '+JSON.stringify(localStorage.msg));
    parent.postMessage(JSON.stringify(localStorage), "*");

    window.onmessage = function(event) {

        if(event.origin !== 'http://minus.framalab.org') {
            alert('La cage est fermée.');
            return;
        }

        alert('La cage est ouverte.');

        var payload = JSON.parse(event.data);
        alert('Cortex entend : '+JSON.stringify(payload.msg));

        if(JSON.stringify(payload.msg).indexOf('Cortex') > -1 ) {
            localStorage.setItem('msg', 'La même chose que chaque nuit, Minus. Tenter de conquérir le monde !');

        } else {
            localStorage.setItem('msg', 'The same thing we do every night, Pinky - try to take over the world!');
        }
        alert('Cortex enregistre le message dans localStorage');
        $('#msg').text(localStorage.msg);
        alert('Cortex dit : '+localStorage.msg);
        parent.postMessage(JSON.stringify(localStorage), "*");

    }
});